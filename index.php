<?php

$content_rss = file_get_contents('https://www.liga.net/tech/technology/rss.xml');
$items_rss = new SimpleXMLElement($content_rss);
foreach ($items_rss->channel->item as $key => $item_rss) {

    $link = preg_replace('/[a-z]+\.liga\.net/', 'dev-tech.com', $item_rss->link);

    echo '<b>' . $item_rss->title . '</b> - ' .  $link . '<br>';
    echo '<img src="'.$item_rss->enclosure['url'].'"><br>';
    echo '<hr>';
}